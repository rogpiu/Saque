package com.itau.saqueinternacional;

public class ListaSaque {

	protected String usuario;
	protected double valorSaque;
	protected String moeda;
	
	public ListaSaque(String usuario, double valorSaque, String moeda) {
		this.usuario = usuario;
		this.valorSaque = valorSaque;
		this.moeda = moeda;
	}

	@Override
	public String toString() {
		return "ListaSaque [usuario=" + usuario + ", valorSaque=" + valorSaque + ", moeda=" + moeda + "]";
	}
	
}
