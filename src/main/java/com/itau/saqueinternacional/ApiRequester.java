package com.itau.saqueinternacional;

import com.github.kevinsawicki.http.HttpRequest;

public class ApiRequester {

	
	public static String request(String url) {	
     String response = HttpRequest.get(url).body();    
     return response;
     
	}
}
