package com.itau.saqueinternacional;

import java.util.HashMap;

public class RatesResponse {

	public String date;
	public String base;
	public HashMap<String, Double> rates;
	public boolean sucesse;
	public int timestamp;
	
	public double conversao(double valor, String moeda) {
		return valor / rates.get(moeda);
	}

	@Override
	public String toString() {
		return "RatesResponse [date=" + date + ", base=" + base + ", rates=" + rates + ", sucesse=" + sucesse
				+ ", timestamp=" + timestamp + "]";
	}
	
	
	
	
}
