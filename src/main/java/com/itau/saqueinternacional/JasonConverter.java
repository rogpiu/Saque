package com.itau.saqueinternacional;


import com.google.gson.Gson;

public class JasonConverter {

	public static RatesResponse deJason(String json) {
		
    	Gson  gson = new Gson();
    	RatesResponse ratesResponse = gson.fromJson(json, RatesResponse.class);
    	return ratesResponse;
	}
	
}
