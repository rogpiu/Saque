package com.itau.saqueinternacional;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileManager {

	public static List<String> lerArquivo(String caminho) {
		Path path = Paths.get(caminho);
		List<String> lista;
		try {
			lista = Files.readAllLines(path);
			
		} catch (IOException e) {
			return null;
		}
		lista.remove(0);
		return lista;
		
	}
	
	public static void gravarArquivo(String conteudo, String caminho) {
		Path caminhoSaida = Paths.get(caminho); 
		
		try {
			Files.write(caminhoSaida, conteudo.getBytes());
		} catch (IOException e) {
			System.out.println("Nao foi possivel gravar o arquivo");
		}
	}
	
}
