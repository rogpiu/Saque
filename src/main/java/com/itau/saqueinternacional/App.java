package com.itau.saqueinternacional;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
    	List<String> lista =  FileManager.lerArquivo("src/accounts.csv");
    	
    	if (lista == null) {
    		System.out.println("Nao foi possivel ler o arquivo");
    		return;
    	}
    	
    	ArrayList<Customer> customer = new ArrayList<Customer>();
    	
    	for (String linha: lista) {
    		String[] partes = linha.split(",");
    		
    		Customer cliente;
    		
    		if (partes[4].equals("basic")){
    		    cliente = new BasicCustomer(partes[0], partes[1], partes[2], Double.parseDouble(partes[3]));
    		}else {
    			cliente = new PremiumCustomer(partes[0], partes[1], partes[2], Double.parseDouble(partes[3]));	
    		}
    		
    		customer.add(cliente);
    	}
    	
    	for (Customer clientes : customer) {
    		System.out.println("Cliente: " + clientes.toString());
    	}
    	
    	
    	// lista de saques
    	
    
    	List<String> clientesSaque =  FileManager.lerArquivo("src/withdrawals.csv");
    	
    	if (clientesSaque == null) {
    		System.out.println("Nao foi possivel ler o arquivo");
    		return;
    	}
    	
    	
		String response = ApiRequester.request("http://data.fixer.io/api/latest?access_key=578ac533a860ac0f3d508e3dbad0ef57");
		
		RatesResponse  rates =   JasonConverter.deJason(response);
		
		
    	ArrayList<ListaSaque> listaSaque = new ArrayList<ListaSaque>();
    	
    	for (String linha: clientesSaque) {
    		String[] partes = linha.split(",");
    		
    		ListaSaque saque;
    		
    		
    		double valorConvertido = rates.conversao(Double.parseDouble(partes[1]), partes[2]);
    		
    		saque = new ListaSaque(partes[0], valorConvertido, partes[2]);
    		
//    		System.out.println("Rates: " + rates.toString());
    		
    		listaSaque.add(saque);
    	}
    	
    	for (ListaSaque saque : listaSaque) {
    		System.out.println("Saque: " + saque.toString());
    	}
    	
    	
    	for (int i=0; i<customer.size(); i++) {
    		for (int x=0; x<listaSaque.size(); x++) {
    			
    			if (customer.get(i).usuario.equals(listaSaque.get(x).usuario)) {

                		System.out.println("Cliente antes: " + customer.get(i).usuario + customer.get(i).saldo);
                		
                		boolean tentativaSaque = customer.get(i).saque(listaSaque.get(x).valorSaque);
                		
                        if (!tentativaSaque) {
                        	System.out.println("Saldo Insuficiente: " + customer.get(i).usuario + customer.get(i).saldo);
                        }
                		System.out.println("Cliente depois: " + customer.get(i).usuario + customer.get(i).saldo);	
    			}
    		}
    	}
    	
    	String gravar = null;
    	for (Customer clientes: customer) {
    		gravar += clientes.toString() + "\n";
    	}
    	
    	FileManager.gravarArquivo(gravar, "src/teste.csv");
//    	for (Customer clientes : customer) {
//		System.out.println("Cliente: " + clientes.toString());
//	}
    	
    	
    	
    	
    }
}
